# Databricks notebook source
# File location and type
file_location = "/FileStore/tables/nba_dirty.csv"
file_type = "csv"

# CSV options
infer_schema = "true"
first_row_is_header = "true"
delimiter = ","

# The applied options are for CSV files. For other file types, these will be ignored.
nba = spark.read.format(file_type) \
  .option("inferSchema", infer_schema) \
  .option("header", first_row_is_header) \
  .option("sep", delimiter) \
  .load(file_location)

display(nba)

# COMMAND ----------

#convert dataframe to Pandas df/establish current date
import pandas as pd
import datetime as dt


# COMMAND ----------

nba = nba.toPandas()
orig_count = len(nba)
orig_count

# COMMAND ----------

now = dt.datetime.now()
now = str(now)
curr_date = now[:10]

# COMMAND ----------

#check for nulls in the following columns
bad_records_null = nba[nba['ID'].isnull() | nba['Name'].isnull()|nba['Team'].isnull() | nba['Number'].isnull() | 
                  nba['Position'].isnull()|nba['Age'].isnull()|nba['Weight'].isnull()|nba['College'].isnull()|
                  nba['Salary'].isnull()]

#convert bad_records_null  to spark dataframe 
bad_records_null_spdf = spark.createDataFrame(bad_records_null)

#add bad_records_null to bad_records
bad_records = spark.createDataFrame(bad_records_null)


bad_records.count()


# COMMAND ----------

#drop rows with null vals from cleaned df if nulls exist in columns below
nba.dropna(subset=['ID','Name','Team','Number','Position','Age','Weight','Salary', 'College'], inplace=True)

# COMMAND ----------

#Check ID Column for bad records 

bad_rec = []
for i in nba['ID']:
    a = list(nba['ID']).index(i)
    if i.isdigit() == False:
        bad_rec.append(a)
        
nba.iloc[bad_rec]  


# COMMAND ----------

# Write bad records in ID column to bad_records.csv
bad_rec_id = pd.DataFrame(nba.iloc[bad_rec])

#convert bad_rec_id to spark dataframe 
bad_rec_id_spdf  = spark.createDataFrame(bad_rec_id)

#add bad_rec_id_spdf to bad_records
bad_records = bad_records.unionAll(bad_rec_id_spdf)

#Drop bad records from cleaned df
bad_rec = bad_rec_id.index.values
nba.drop(bad_rec, inplace=True)

#Set ID dtype to int
#nba['ID'] = nba['ID'].astype('int')


bad_records.count()

# COMMAND ----------

#check Name field for numbers (bad records)
bad_rec = []
for i in nba['Name']:
    a = list(nba['Name']).index(i)
    if i.isdigit() == True:
        bad_rec.append(a)
        
nba.iloc[bad_rec]  

# COMMAND ----------

#write bad name recs to bad_records.csv
bad_rec_name = pd.DataFrame(nba.iloc[bad_rec])

#Drop bad records from cleaned df
bad_rec = bad_rec_name.index.values
nba.drop(bad_rec, inplace=True)

#convert bad_rec_name to spark dataframe to be written out to bad_records.csv
bad_rec_nm_spdf  = spark.createDataFrame(bad_rec_name)

# add bad_rec_nm_spdf to bad_records
bad_records = bad_records.unionAll(bad_rec_nm_spdf)

#display(bad_rec_nm_spdf)

bad_records.count() #102

# COMMAND ----------

#check Number field for alpha (bad records)
bad_rec = []
for i in nba['Number']:
    a = list(nba['Number']).index(i)
    if i.isdigit() == False :
        bad_rec.append(a)

#write bad number recs df
bad_rec_number = pd.DataFrame(nba.iloc[bad_rec])
        
nba.iloc[bad_rec]  

#Drop bad records from cleaned df
bad_rec = bad_rec_number.index.values
nba.drop(bad_rec, inplace=True)

#Set ID dtype to int
nba['Number'] = nba['Number'].astype('int')


#check Number field for numbers greater than 99 (bad records)
bad_rec = []
for i in nba['Number']:
    a = list(nba['Number']).index(i)
    if i > 99 :
        bad_rec.append(a)

    
#write bad number recs to bad_records.csv
bad_rec_number_gt = pd.DataFrame(nba.iloc[bad_rec])

#Drop bad records from cleaned df
bad_rec = bad_rec_number_gt.index.values
nba.drop(bad_rec, inplace=True)

#add second criteria for bad number records to bad_rec_number df
bad_rec_number = bad_rec_number.append(bad_rec_number_gt)


#bad_rec_number

# COMMAND ----------

bad_rec_number['Number'] = bad_rec_number['Number'].astype('str')

#convert bad_rec_number to spark dataframe 
bad_rec_num_spdf  = spark.createDataFrame(bad_rec_number)

# add bad_rec_num_spdf to bad_records
bad_records = bad_records.unionAll(bad_rec_num_spdf)

bad_records.count() #24

# COMMAND ----------

#check Positin field for strings greater than 2 / not alpha (bad records)
bad_rec = []
for i in nba['Position']:
    a = list(nba['Position']).index(i)
    if len(i) > 2:
        bad_rec.append(a)
    elif i.isalpha() == False:
        bad_rec.append(a)
        

nba.iloc[bad_rec]  

# COMMAND ----------

#write bad Position recs to df
bad_rec_pos = pd.DataFrame(nba.iloc[bad_rec])

if len(bad_rec_pos) > 0:
  #convert bad_rec_pos to spark dataframe 
  bad_rec_pos_spdf  = spark.createDataFrame(bad_rec_pos)
  
  # add bad_rec_num_spdf to bad_records
  bad_records = bad_records.unionAll(bad_rec_pos_spdf)
  
  #drop bad position records from clean df
  bad_rec = bad_rec_pos.index.values
  nba.drop(bad_rec, inplace=True)

bad_records.count() #26

# COMMAND ----------

#check Age field for alpha (bad records)
bad_rec = []
for i in nba['Age']:
    a = list(nba['Age']).index(i)
    if i.isdigit() == False:
        bad_rec.append(a)
        
        
nba.iloc[bad_rec]  

# COMMAND ----------



#write bad Age recs to bad_records.csv
bad_rec_age = pd.DataFrame(nba.iloc[bad_rec])


if len(bad_rec_age) >0:
  bad_rec_age_spdf  = spark.createDataFrame(bad_rec_age)
  
  # add bad_rec_num_spdf to bad_records
  bad_records = bad_records.unionAll(bad_rec_age_spdf)
  
  #drop bad position records from clean df
  bad_rec = bad_rec_age.index.values
  nba.drop(bad_rec, inplace=True)

bad_records.count() #106



# COMMAND ----------

bad_rec = []
for i in nba['Weight']:
    a = list(nba['Weight']).index(i)
    if i.isdigit() == False:
        bad_rec.append(a)
        
#add bad Wieght recs to df        
bad_rec_wt = pd.DataFrame(nba.iloc[bad_rec])

#Drop bad records from cleaned df
bad_rec = bad_rec_wt.index.values
nba.drop(bad_rec, inplace=True)

nba['Weight'] = nba['Weight'].astype('int')

#check Weight field for unrealistic weight(bad records)
bad_rec = []
for i in nba['Weight']:
    a = list(nba['Weight']).index(i)
    if i > 500:
        bad_rec.append(a)
    elif i < 100:
        bad_rec.append(a)
        
        
#write bad records to df       
bad_rec_wt_unreal = pd.DataFrame(nba.iloc[bad_rec])

#Drop bad records from cleaned df
bad_rec = bad_rec_wt_unreal.index.values
nba.drop(bad_rec, inplace=True)

#add additional criteria records to bad_rec_wt
bad_rec_wt = bad_rec_wt.append(bad_rec_wt_unreal)

#revert nba[weight] to  str
nba['Weight'] = nba['Weight'].astype('str')

#set bad_rec_wt[weight] to  str
bad_rec_wt['Weight'] = bad_rec_wt['Weight'].astype('str')

if len(bad_rec_wt) > 0:
  bad_rec_wt_spdf  = spark.createDataFrame(bad_rec_wt)
  
  # add bad_rec_num_spdf to bad_records
  bad_records = bad_records.unionAll(bad_rec_wt_spdf)

bad_records.count() #109

# COMMAND ----------

#check College field for digits (bad records)
bad_rec = []
for i in nba['College']:
    a = list(nba['College']).index(i)
    if i.isdigit() == True:
        bad_rec.append(a)
        
       
#write bad records to df       
bad_rec_col = pd.DataFrame(nba.iloc[bad_rec])

#Drop bad records from cleaned df
bad_rec = bad_rec_col.index.values
nba.drop(bad_rec, inplace=True)

if len(bad_rec_col) > 0:
  bad_rec_col_spdf  = spark.createDataFrame(bad_rec_col)
  
  # add bad_rec_num_spdf to bad_records
  bad_records = bad_records.unionAll(bad_rec_col_spdf)

bad_records.count() #110


# COMMAND ----------

#check  Salary field for alpha (bad records)
bad_rec = []
for i in nba['Salary']:
    a = list(nba['Salary']).index(i)
    if i.isdigit() == False:
        bad_rec.append(a)
        
        
#write bad Salary recs to bad_records.csv
bad_rec_sal = pd.DataFrame(nba.iloc[bad_rec])

#Drop bad records from cleaned df
bad_rec = bad_rec_sal.index.values
nba.drop(bad_rec, inplace=True)

#check  Salary field for unrealistic sals (bad records)
nba['Salary'] = nba['Salary'].astype('float')
bad_rec = []
for i in nba['Salary']:
    a = list(nba['Salary']).index(i)
    if i > 30000000:
        bad_rec.append(a)
    elif i < 15000:
        bad_rec.append(a)

#write ad records to df
bad_rec_sal_unreal = pd.DataFrame(nba.iloc[bad_rec])

#remove unrealistic salary records from clean df
bad_rec = bad_rec_sal_unreal.index.values
nba.drop(bad_rec, inplace=True)

#add unrealistic sal records to bad ral records df
bad_rec_sal = bad_rec_sal.append(bad_rec_sal_unreal)

#revert dataype chance to salary field
nba['Salary'] = nba['Salary'].astype('str')
bad_rec_sal['Salary'] = bad_rec_sal['Salary'].astype('str')

if len(bad_rec_sal) > 0:
  bad_rec_sal_spdf  = spark.createDataFrame(bad_rec_sal)
  
  # add bad_rec_num_spdf to bad_records
  bad_records = bad_records.unionAll(bad_rec_sal_spdf)

bad_records.count() #113

# COMMAND ----------

clean_df =  spark.createDataFrame(nba)
#clean_df.printSchema()

# COMMAND ----------

if orig_count == clean_df.count() + bad_records.count():
  print("All records accounted for")
else:
  print('orig_count: '+ str(orig_count) + '   bad: ' + str(bad_records.count()) + '  clean: ' + str(clean_df.count()))

# COMMAND ----------

if orig_count == clean_df.count() + bad_records.count():
  #remove the bad_records file for the current date if it exists
  dbutils.fs.rm("dbfs:/FileStore/tables/bad_records_"+curr_date+".csv", True)

  #remove the clean_records file for the current date if it exists
  dbutils.fs.rm("dbfs:/FileStore/tables/clean_records_"+curr_date+".csv", True)

  #write bad_records to csv in file system
  bad_records.repartition(1).write.csv(path="dbfs:/FileStore/tables/bad_records_"+curr_date+".csv", header="true")

  #write clean_records to csv in file system
  clean_df.repartition(1).write.csv(path="dbfs:/FileStore/tables/clean_records_"+curr_date+".csv", header="true")
  print("Files written successfully!")
else:
  print('Error - check messages in console')


# COMMAND ----------

bad = spark.read.format(file_type).option("inferSchema", "true").option("header", "true").load("dbfs:/FileStore/tables/bad_records_"+curr_date+".csv")

display(bad)

# COMMAND ----------

clean = spark.read.format(file_type).option("inferSchema", "true").option("header", "true").load("dbfs:/FileStore/tables/clean_records_"+curr_date+".csv")

display(clean)